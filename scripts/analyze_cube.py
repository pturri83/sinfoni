"""Program to analyze SINS/zC-SINF data cubes.

References:
    [ref1] Greisen et Calabretta, 2002
"""


import glob
from os import makedirs, path

from astropy.io import fits
from matplotlib import pyplot as plt
import numpy as np

from source import cube_nifs, cube_sinfoni, params_nifs, params_sinfoni,\
    fit_sinfoni


# Data parameters
folder = '/Volumes/Astro/Data/GIRMOS/SINFONI'  # Parent folder

# Analysis parameters
rad_b = 1  # Beam radius (")
k_buffer = 10  # Fraction of the wavelength range to cut from both ends of the
# collapsed data cube
conv_spxs = [1, 2, 3, 4]  # Radii of the convolution kernel (px)
vel_bin = 34  # Velocity binning (km/s)
vel_range = [-500, 500]  # Velocity range [min, max] (km/s)
vel_bin_spx = 2  # Spaxel velocity binning

# Graphic parameters
test = None  # Name of the only object to analyze
sp_ticks = 0.5  # Distance of the spaxel axes' ticks (")
mask_vel = 1e-22  # Minimum H_a line flux to plot its velocity (W/m^2)
vel_int = [-1e-19, 2.3e-19]  # Range of flux densities of the velocity binning
# (W/m^2/um)
vel_int_bin = [-2e-19, 4.1e-19]  # Range of flux densities of the velocity
# binning, with binned spaxels (W/m^2/um)

# Manage files
print("\nProgram started\n")
plot_f = path.join(folder, 'Plots', 'Cubes')
fits_f = path.join(folder, 'Plots', 'Cubes', 'FITS')

if not path.exists(plot_f):
    makedirs(plot_f)

if not path.exists(fits_f):
    makedirs(fits_f)

# Calculate telluric lines
sky = params_nifs.sky_compile(params_nifs.sky_c, params_nifs.sky_w[0])

# Select target
gal_names = params_sinfoni.gal_names
gal_zs = params_sinfoni.gal_zs
r_ap = params_sinfoni.r_ap
f_ha = params_sinfoni.f_ha
files = glob.glob(path.join(folder, 'Data', 'Cubes', '*_data_cut.fits'))
n_files = len(files)
sps = params_sinfoni.ps
ap = np.zeros([n_files, 2])

for i_file in range(n_files):

    # Load data
    filename = files[i_file]
    filename_base = path.basename(filename)
    filenames = filename_base.split('_', 1)
    obj = filenames[0]

    if test:
        if obj != test:
            continue

    print("Target: " + obj + " ...")
    hdul = fits.open(filename)
    header = hdul[0].header
    data = hdul[0].data
    data *= params_sinfoni.wm2_ergscm2
    hdul.close()
    filename_psf = filename[:-13] + 'psf.fits'
    hdul = fits.open(filename_psf)
    data_psf = hdul[0].data
    hdul.close()

    # Prepare redshift
    gal_idx = np.where(np.array(gal_names) == obj)[0][0]
    gal_z = gal_zs[gal_idx]

    # Show PSF
    cube_nifs.plot_img(data_psf, sps, sp_ticks, c_label="I",
                       title="{0} PSF".format(obj),
                       save=path.join(plot_f, '{0}_psf.png'.format(obj)))

    # Calculate wavelengths
    slice_idx = np.arange(data.shape[0])
    wvl = ((slice_idx + 1 - header['CRPIX3']) * header['CDELT3']) + \
        header['CRVAL3']  # [ref1]

    # Clean data
    wvl_buffer = (np.max(wvl) - np.min(wvl)) / k_buffer
    k_min = np.min(wvl) + wvl_buffer
    k_max = np.max(wvl) - wvl_buffer
    wvl_clean, data_clean = cube_nifs.cut_cube(wvl, data, k_min, k_max)
    wvl_clean, data_clean = cube_nifs.clean_cube(wvl_clean, data_clean,
                                                 params_nifs.sky_c,
                                                 params_nifs.sky_w[1])

    # Show collapsed data cube
    data_coll = cube_nifs.collapse_cube(data_clean)
    data_coll *= (wvl_clean[-1] - wvl_clean[0])
    cube_nifs.plot_img(data_coll, sps, sp_ticks,
                       c_label=("Flux (" + params_sinfoni.flux_unit_erg + ")"),
                       title="{0} collapsed cube".format(obj),
                       save=path.join(plot_f, '{0}_coll.png'.format(obj)))

    # Select beam
    direction = [(data_coll.shape[1] / 2), (data_coll.shape[0] / 2),
                 (rad_b / sps)]
    beam = cube_nifs.beam(data, direction[0: -1], (rad_b / sps))
    beam_bin = cube_nifs.bin_spx(data, beam[1], beam[0])

    # Show beam
    cube_nifs.plot_img(data_coll, sps, sp_ticks,
                       beam_circ=np.array([direction]),
                       c_label=("Flux (" + params_sinfoni.flux_unit_erg + ")"),
                       title="{0} collapsed cube with beam".format(obj),
                       save=path.join(plot_f, '{0}_coll_beam.png'.format(obj)))

    # Fit beam
    beam_fit = fit_sinfoni.fit_spec(wvl, beam_bin, gal_z)

    # Show the beam spectrum, fitted
    cube_sinfoni.plot_spectrum(wvl, beam_bin, sky=sky, fit_mod=beam_fit,
                               y_label=("Flux density (" +
                                        params_nifs.flux_dens_unit + ")"),
                               title="{0} beam spectrum fitted".format(obj),
                               save=path.join(plot_f,
                                              '{0}_beam_spectr_fit.png'.format(
                                                  obj)))

    # Map lines
    maps = fit_sinfoni.fit_map(wvl, data, gal_z)
    maps_plot = fit_sinfoni.fit_map(wvl, data, gal_z, constrain_peak=True)

    # Show maps
    cube_sinfoni.plot_map(maps_plot, sps, sp_ticks, 'ha_i',
                          save=path.join(plot_f,
                                         '{0}_map_ha_i.png'.format(obj)))
    cube_sinfoni.plot_map(maps_plot, sps, sp_ticks, 'ha_s',
                          save=path.join(plot_f,
                                         '{0}_map_ha_s.png'.format(obj)))
    cube_sinfoni.plot_map(maps_plot, sps, sp_ticks, 'ha_c', mask_map='ha_i',
                          mask_thr=mask_vel, vel=True,
                          save=path.join(plot_f,
                                         '{0}_map_ha_c.png'.format(obj)))

    # Save maps as FITS files
    map_ha_i, _ = cube_sinfoni.grab_map(maps, 'ha_i')
    map_ha_s, _ = cube_sinfoni.grab_map(maps, 'ha_s')
    map_ha_c, _ = cube_sinfoni.grab_map(maps, 'ha_c')
    hdu_ha_i = fits.PrimaryHDU(map_ha_i)
    hdu_ha_s = fits.PrimaryHDU(map_ha_s)
    hdu_ha_c = fits.PrimaryHDU(map_ha_c)
    hdu_ha_i.writeto(path.join(fits_f, '{0}_map_ha_i.fits'.format(obj)),
                     overwrite=True)
    hdu_ha_s.writeto(path.join(fits_f, '{0}_map_ha_s.fits'.format(obj)),
                     overwrite=True)
    hdu_ha_c.writeto(path.join(fits_f, '{0}_map_ha_c.fits'.format(obj)),
                     overwrite=True)

    # Measure aperture photometry
    ap_center = np.array(map_ha_i.shape) / 2
    beam_m = cube_nifs.beam(data, [ap_center[1], ap_center[0]],
                            (r_ap[gal_idx] / sps))
    beam_bin_m = cube_nifs.bin_map(maps, 'ha_i', beam_m)
    ap[i_file, 0] = beam_bin_m[0]
    ap[i_file, 1] = f_ha[gal_idx] * 1e-17

    # Use different convolution radii
    for conv_spx in conv_spxs:

        # Convolve maps
        maps_conv = cube_sinfoni.convolve_maps(maps, conv_spx)
        maps_conv_plot = cube_sinfoni.convolve_maps(maps_plot, conv_spx)

        # Show convolved maps
        cube_sinfoni.plot_map(maps_conv_plot, sps, sp_ticks, 'ha_i',
                              save=path.join(plot_f,
                                             ('{0}_map_ha_i_conv_{1}' +
                                              '.png').format(obj, conv_spx)))
        cube_sinfoni.plot_map(maps_conv_plot, sps, sp_ticks, 'ha_s',
                              mask_map='ha_i', mask_thr=(mask_vel * 1e-20),
                              save=path.join(plot_f,
                                             ('{0}_map_ha_s_conv_{1}' +
                                              '.png').format(obj, conv_spx)))
        cube_sinfoni.plot_map(maps_conv_plot, sps, sp_ticks, 'ha_c',
                              mask_map='ha_i', mask_thr=(mask_vel * 1e-20),
                              vel=True,
                              save=path.join(plot_f,
                                             ('{0}_map_ha_c_conv_{1}' +
                                              '.png').format(obj, conv_spx)))

        # Save convolved maps as FITS files
        map_conv_ha_i, _ = cube_sinfoni.grab_map(maps_conv, 'ha_i')
        map_conv_ha_s, _ = cube_sinfoni.grab_map(maps_conv, 'ha_s')
        map_conv_ha_c, _ = cube_sinfoni.grab_map(maps_conv, 'ha_c')
        hdu_conv_ha_i = fits.PrimaryHDU(map_conv_ha_i)
        hdu_conv_ha_s = fits.PrimaryHDU(map_conv_ha_s)
        hdu_conv_ha_c = fits.PrimaryHDU(map_conv_ha_c)
        hdu_conv_ha_i.writeto(
            path.join(fits_f, '{0}_map_conv_{1}_ha_i.fits'.format(obj,
                                                                  conv_spx)),
            overwrite=True)
        hdu_conv_ha_s.writeto(
            path.join(fits_f, '{0}_map_conv_{1}_ha_s.fits'.format(obj,
                                                                  conv_spx)),
            overwrite=True)
        hdu_conv_ha_c.writeto(
            path.join(fits_f, '{0}_map_conv_{1}_ha_c.fits'.format(obj,
                                                                  conv_spx)),
            overwrite=True)

    # Analyze velocity channels
    vel, data_vel = cube_sinfoni.bin_vel(wvl, data, params_nifs.h_alpha, gal_z,
                                         vel_bin, vel_range)
    cube_sinfoni.plot_vel(vel, data_vel, sps, sp_ticks, imin=vel_int[0],
                          imax=vel_int[1],
                          save=path.join(plot_f, obj, 'bin_1'))
    data_vel_bin = cube_nifs.bin_cube(data_vel, vel_bin_spx)
    cube_sinfoni.plot_vel(vel, data_vel_bin, sps, sp_ticks,
                          imin=vel_int_bin[0], imax=vel_int_bin[1],
                          save=path.join(plot_f, obj,
                                         'bin_{0}'.format(vel_bin_spx)))

# Analyze aperture photometry
fig = plt.figure(figsize=(7, 7))
ax = fig.add_subplot(111)
ax.set_position([0.15, 0.15, 0.7, 0.7])
plt.plot(ap[:, 0], ap[:, 1], '.', ms=7, c='k')
ax.axis('equal')
x_lim = ax.get_xlim()
y_lim = ax.get_ylim()
plt.plot([0, 1], [0, 1], '--', linewidth=1, c='b')
plt.xlim(x_lim)
plt.ylim(y_lim)
ax.set_xlabel("From my analysis ({0})".format(params_sinfoni.flux_unit_erg))
ax.set_ylabel("From literature ({0})".format(params_sinfoni.flux_unit_erg))
plt.title("Aperture photometry comparison", y=1.05, fontsize=16)
plt.savefig(path.join(plot_f, 'ap_compare.png'))
plt.close()
print("\nProgram terminated\n")
