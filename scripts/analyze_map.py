"""Program to analyze SINS/zC-SINF maps.
"""


from os import makedirs, path
from pathlib import Path
import warnings

from astropy.io import fits

from source import cube_nifs, params_sinfoni


# Data parameters
folder = '/Volumes/Astro/Data/GIRMOS/SINFONI'  # Parent folder

# Graphic parameters
test = None  # Name of the only object to analyze
p_ticks = 0.5  # Distance of the pixel axes' ticks (")

# Manage files
print("\nProgram started\n")
data_f = path.join(folder, 'Data', 'Maps')
plot_f = path.join(folder, 'Plots', 'Maps')

if not path.exists(plot_f):
    makedirs(plot_f)

# Select targets
ps = params_sinfoni.ps
targets = Path(data_f).rglob('*_Ha_fnm.fits')

for i_target in targets:

    # Load files
    foldername = str(i_target.parent)
    filename_flux = i_target.name
    warnings.simplefilter('ignore')
    hdul = fits.open(path.join(foldername, filename_flux))
    warnings.simplefilter('default')
    header = hdul[0].header
    data_flux = hdul[0].data
    hdul.close()
    obj = header['OBJECT']

    if test:
        if obj != test:
            continue

    print("Target: " + obj + " ...")
    filename_vel = filename_flux.replace('fnm', 'vm', 1)
    warnings.simplefilter('ignore')
    hdul = fits.open(path.join(foldername, filename_vel))
    warnings.simplefilter('default')
    data_vel = hdul[0].data
    hdul.close()

    # Plot maps
    cube_nifs.plot_img(data_flux, ps, p_ticks,
                       c_label="Flux density (" + params_sinfoni.flux_dens_unit
                               + ")",
                       title=r"Map of H$_{\alpha}$ peak",
                       save=path.join(plot_f, '{0}_flux.png'.format(obj)))
    cube_nifs.plot_img(data_vel, ps, p_ticks,
                       c_label="Velocity (km/s)",
                       title=r"Map of H$_{\alpha}$ LOS velocity",
                       save=path.join(plot_f, '{0}_vel.png'.format(obj)))

print("\nProgram terminated\n")
