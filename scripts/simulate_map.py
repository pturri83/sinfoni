"""Program to simulate GIRMOS maps from SINS/zC-SINF data cubes.
"""


import glob
from os import makedirs, path

from astropy.io import fits
import numpy as np
from scipy import io

from source import cube_nifs, cube_sinfoni, params_nifs, params_sinfoni, \
    fit_sinfoni


# Data parameters
sinfoni_folder = '/Volumes/Astro/Data/GIRMOS/SINFONI'  # SINFONI parent folder
psf_folder = '/Volumes/Astro/Data/GIRMOS/PSF/uriel'  # PSF folder
psf_ps = 0.0053  # PSF pixel scale ("/px)
psf_side = 240  # PSF side (px)
psf_fov = 120  # PSF grid FOV (")
n_psfs = 11  # Number of PSFs on the grid's side

# Analysis parameters
psf_cells = [[0, 0], [-3, 0], [-5, 0]]  # PSF cells on the grid, respect to the
# center ([x, y])
shrink = 2  # Shrink factor of the data cube
ps_girmos = 0.025  # GIRMOS spaxel scale ("/px)
fov_girmos = 1  # GIRMOS FOV (")
rad_b = 1  # Beam radius (")
conv_spx = 2  # Radius of the convolution kernel (px)

# K20-ID7 parameters
target_name = 'K20-ID7'  # Name of the target
target_clumps = [[26, 8], [31, 14], [34, 23]]  # Approximate centers of the
# target's main clumps [x, y] (px)
target_ap = 0.1  # Aperture radius (")

# Graphic parameters
test = None  # Name of the only object to analyze
sp_ticks = 0.5  # Distance of the spaxel axes' ticks (")

# Select PSF
print("\nProgram started\n")
sps = params_sinfoni.ps
psfs_list = glob.glob(path.join(psf_folder, 'PSF_*.mat'))
psfs_list += ['no_psf']
psfs = None
psfs_center = None

for psf_list in psfs_list:

    # Select type of PSF
    psf_name = path.splitext(path.basename(psf_list))[0]
    print("PSF file: {0}".format(psf_name))

    # No PSF
    if psf_list == psfs_list[-1]:
        psfs[:] = 0
        psfs[psfs_center.astype(int)] = 1
        psf_cells_ok = [[0, 0]]

    # Load PSF
    else:
        psf_struct = io.loadmat(psf_list)
        psfs = psf_struct['frame2D']
        psf_cells_ok = psf_cells

    # Select direction
    for psf_cell in psf_cells_ok:

        # Select PSF
        print("   Cell: x={0}, y={1}".format(psf_cell[0], psf_cell[1]))
        psf_coord_f = 'x{0}_y{1}'.format(psf_cell[0], psf_cell[1])
        plot_f = path.join(sinfoni_folder, 'Plots', 'GIRMOS', 'results',
                           psf_name, psf_coord_f)
        fits_f = path.join(sinfoni_folder, 'Plots', 'GIRMOS', 'results',
                           psf_name, psf_coord_f, 'FITS')

        if not path.exists(plot_f):
            makedirs(plot_f)

        if not path.exists(fits_f):
            makedirs(fits_f)

        psfs_center = np.array(psfs.shape) / 2
        psf_center = psfs_center + (np.array(psf_cell) * psf_side)
        psf = psfs[int(psf_center[1] - (psf_side / 2)):
                   int(psf_center[1] + (psf_side / 2)),
                   int(psf_center[0] - (psf_side / 2)):
                   int(psf_center[0] + (psf_side / 2))]
        psf_coord = np.array(psf_cell) * psf_fov / n_psfs
        psf_coord = psf_coord.astype(int)

        # Show PSF
        cube_nifs.plot_img(psf, psf_ps, sp_ticks, c_label="I",
                           title="PSF ({0}\", {1}\")".format(psf_coord[0],
                                                             psf_coord[1]),
                           save=path.join(plot_f, 'psf.png'))

        # Calculate telluric lines
        sky = params_nifs.sky_compile(params_nifs.sky_c, params_nifs.sky_w[0])

        # Select target
        gal_names = params_sinfoni.gal_names
        gal_zs = params_sinfoni.gal_zs
        files = glob.glob(path.join(sinfoni_folder, 'Data', 'Cubes',
                                    '*_data_cut.fits'))
        n_files = len(files)

        for i_file in range(n_files):

            # Load data
            filename = files[i_file]
            filename_base = path.basename(filename)
            filenames = filename_base.split('_', 1)
            obj = filenames[0]

            if test:
                if obj != test:
                    continue

            print("      Target: {0}".format(obj))
            hdul = fits.open(filename)
            header = hdul[0].header
            data = hdul[0].data
            hdul.close()

            # Prepare redshift
            gal_idx = np.where(np.array(gal_names) == obj)[0][0]
            gal_z = gal_zs[gal_idx]

            # Calculate wavelengths
            slice_idx = np.arange(data.shape[0])
            wvl = ((slice_idx + 1 - header['CRPIX3']) * header['CDELT3']) + \
                header['CRVAL3']  # [ref1]

            # Map lines
            maps = fit_sinfoni.fit_map(wvl, data, gal_z)

            # Show maps
            cube_sinfoni.plot_map(maps, sps, sp_ticks, 'ha_i', perc=0.2,
                                  save=path.join(plot_f,
                                                 '{0}_map_ha_i.png'.format(
                                                     obj)))

            # Scale maps
            maps_scale = cube_sinfoni.scale_maps(maps, sps, (psf_ps * shrink))

            # Show scaled maps
            cube_sinfoni.plot_map(maps_scale, psf_ps, sp_ticks, 'ha_i',
                                  perc=0.2,
                                  save=path.join(plot_f,
                                                 ('{0}_map_ha_i_scale.' +
                                                  'png').format(obj)))

            # Convolve maps
            maps_conv = cube_sinfoni.convolve_maps_psf(maps_scale, psf)

            # Show convolved maps
            cube_sinfoni.plot_map(maps_conv, psf_ps, sp_ticks, 'ha_i',
                                  perc=0.2,
                                  save=path.join(plot_f,
                                                 ('{0}_map_ha_i_conv' +
                                                  '.png').format(obj)))

            # Scale back maps
            maps_back = cube_sinfoni.scale_maps(maps_conv, psf_ps, ps_girmos)

            # Show scaled back maps
            cube_sinfoni.plot_map(maps_back, ps_girmos, sp_ticks, 'ha_i',
                                  perc=0.2,
                                  save=path.join(plot_f,
                                                 ('{0}_map_ha_i_back' +
                                                  '.png').format(obj)))

            # Cut maps
            maps_center = np.array(maps_back.shape) / 2
            maps_center = maps_center.astype(int)
            maps_side2 = int(fov_girmos / ps_girmos / 2)
            maps_cut = maps_back[(maps_center[0] - maps_side2):
                                 (maps_center[0] + maps_side2),
                                 (maps_center[1] - maps_side2):
                                 (maps_center[1] + maps_side2)]

            # Show cut maps
            cube_sinfoni.plot_map(maps_cut, ps_girmos, sp_ticks, 'ha_i',
                                  perc=0.2,
                                  save=path.join(plot_f,
                                                 '{0}_map_ha_i_cut.png'.format(
                                                     obj)))

            # Save cut maps as FITS files
            map_ha_i_cut, _ = cube_sinfoni.grab_map(maps_cut, 'ha_i')
            hdu_ha_i_cut = fits.PrimaryHDU(map_ha_i_cut)
            hdu_ha_i_cut.writeto(
                path.join(fits_f, '{0}_map_ha_i_cut.fits'.format(obj)),
                overwrite=True)

            # Test the target
            if obj == target_name:

                # Measure the apertures flux
                target_map, _ = cube_sinfoni.grab_map(maps_cut, 'ha_i')
                mesh_x, mesh_y = np.meshgrid(np.arange(target_map.shape[1]),
                                             np.arange(target_map.shape[0]))
                metr_file = open(path.join(plot_f, '{0}_metr.txt'.format(obj)),
                                 'w')
                header_str = "Aperture radius: {:.3f}\"\n\n".format(target_ap)
                metr_file.write(header_str)
                i_target = 0

                for target_clump in target_clumps:
                    i_target += 1
                    mesh_dist = np.hypot((mesh_x - target_clump[0]),
                                         (mesh_y - target_clump[1]))
                    target_flux = np.sum(target_map[np.where(mesh_dist <=
                                                             (target_ap /
                                                              ps_girmos))])
                    peak = target_map[target_clump[1], target_clump[0]]
                    name_str = "Clump #{0}\n".format(i_target)
                    coord_str = "   x: {0:d} px, y: {1:d} px\n".format(
                        target_clump[0], target_clump[1])
                    ap_str = "   Aperture flux: {0:.3e} {1}\n".format(
                        target_flux, params_sinfoni.flux_unit_txt)
                    peak_str = "   Peak flux: {0:.3e} {1}\n\n".format(
                        peak, params_sinfoni.flux_unit_txt)
                    metr_file.write(name_str)
                    metr_file.write(coord_str)
                    metr_file.write(ap_str)
                    metr_file.write(peak_str)

                # Show apertures map
                metr_file.close()
                beams = np.array(target_clumps)
                cube_sinfoni.plot_map(maps_cut, ps_girmos, sp_ticks, 'ha_i',
                                      beam_circ=np.concatenate(
                                          (np.array(beams), np.rot90(np.array(
                                              [[(target_ap / ps_girmos)] *
                                               len(target_clumps)]))), axis=1),
                                      save=path.join(plot_f,
                                                     ('{0}_map_ha_i_ap' +
                                                      '.png').format(obj)))

print("\nProgram terminated\n")
