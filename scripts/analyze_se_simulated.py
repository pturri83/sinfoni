"""Program to analyze the SExtractor output of the H_a maps from the
simulated GIRMOS maps from SINS/zC-SINF data cubes.
"""


import glob
from os import makedirs, path, walk
import pickle
import re

from astropy import table, units
from astropy.io import ascii, fits
from matplotlib import pyplot as plt
import numpy as np

from source import cube_nifs, params_sinfoni


# Data parameters
folder_parent = '/Volumes/Astro/Data/GIRMOS/SINFONI/Plots/GIRMOS'  # Parent
# folder

# Graphic parameters
ticks = 0.5  # Distance of the spaxel axes' ticks (")
bin_flux_auto = 1e-17  # Width of the bins in the integrated flux function
# (erg/s/cm^2)
bin_flux_2x2 = 5e-19  # Width of the bins in the aperture flux function
# (erg/s/cm^2)
bin_fwhm = 0.05  # Width of the bins in the FWHM function (")
area_max = 1.5  # Maximum isophotal area plotted (arcsec^2)
fwhm_max = 1.4  # Maximum FWHM plotted (")
flux_max = 3e-16  # Maximum isophotal flux (erg/s/cm^2)

# Select PSF
print("\nProgram started\n")
folder_parent = path.join(folder_parent, 'results')

for subfolder1 in next(walk(folder_parent))[1]:

    # Select direction
    print("PSF file: {0}".format(subfolder1))

    for subfolder2 in next(walk(path.join(folder_parent, subfolder1)))[1]:

        # Manage files
        psf_cell = re.split('[x_y]', subfolder2)
        print("   Cell: x={0}, y={1}".format(psf_cell[1], psf_cell[3]))
        folder = path.join(folder_parent, subfolder1, subfolder2, 'FITS')
        plot_f = path.join(folder, 'Plots')

        if not path.exists(plot_f):
            makedirs(plot_f)

        files = glob.glob(path.join(folder, '*.fit'))
        n_files = len(files)
        n_file = 0
        data_stack = []
        catalog = table.Table(names=('target', 'x', 'y', 'fwhm_gauss',
                                     'fwhm_deconv', 'iso_area', 'flux_auto',
                                     'flux_2x2', 'bkg'),
                              dtype=('S', 'float', 'float', 'float', 'float',
                                     'float', 'float', 'float', 'float'))
        unit_pos = units.astrophys.pix
        unit_len = units.si.arcsec
        unit_area = units.si.arcsec ** 2
        unit_flux = units.cgs.erg / units.si.s / (units.si.cm ** 2)
        unit_spectr = units.si.micron
        unit_speed = units.si.m / units.si.s
        catalog['x'].unit = unit_pos
        catalog['y'].unit = unit_pos
        catalog['fwhm_gauss'].unit = unit_len
        catalog['fwhm_deconv'].unit = unit_len
        catalog['iso_area'].unit = unit_area
        catalog['flux_auto'].unit = unit_flux
        catalog['flux_2x2'].unit = unit_flux
        catalog['bkg'].unit = unit_flux

        # Select target
        for i_file in range(n_files):

            # Load data
            n_file += 1
            filename = files[i_file]
            filename_base = path.basename(filename)
            filenames = filename_base.split('.', 1)
            filename_root = filenames[0]
            objs = filename_base.split('_', 1)
            obj = objs[0]
            print("      Target: {0}".format(obj))
            data = table.Table.read(filename)
            image_file = path.join(folder, (filename_root + '.fits'))
            hdul = fits.open(image_file)
            img = hdul[0].data
            img_header = hdul[0].header
            hdul.close()

            # Prepare data for plotting
            models_table = data['X_IMAGE', 'Y_IMAGE', 'A_IMAGE', 'B_IMAGE',
                                'THETA_IMAGE']
            models_array = np.array(models_table)
            models = []

            for i_model in models_array:
                model = list(i_model)
                model[0] = model[0] - 1
                model[1] = model[1] - 1
                model[2] = model[2] * 2 * np.sqrt(2 * np.log(2))
                model[3] = model[3] * 2 * np.sqrt(2 * np.log(2))
                model.insert(len(model), (model[2] + model[3]) / 2)
                models.insert(len(model), model)

            if len(models_table) == 0:
                model_plot = None
            else:
                model_plot = models

            # Plot image
            x_mesh, y_mesh = np.meshgrid(np.arange(img.shape[0]),
                                         np.arange(img.shape[1]))
            cube_nifs.plot_img(img, params_sinfoni.ps, ticks, model=model_plot,
                               c_label=params_sinfoni.flux_unit, title=obj,
                               save=path.join(plot_f, '{0}.png'.format(obj)))

            # Compile catalog
            for i_target in data:

                # Find positions
                x = i_target['X_IMAGE'] - 1
                y = i_target['Y_IMAGE'] - 1
                x_min = int(np.floor(x))
                y_min = int(np.floor(y))
                fwhm = i_target['FWHM_IMAGE'] * params_sinfoni.ps
                fwhm_psf_idx = [i for i, s in
                                enumerate(params_sinfoni.gal_names) if
                                obj == s][0]
                fwhm_psf = params_sinfoni.gal_fwhm[fwhm_psf_idx]
                fwhm_deconv2 = (fwhm ** 2) - (fwhm_psf ** 2)

                if fwhm_deconv2 > 0:
                    fwhm_deconv = np.sqrt(fwhm_deconv2)
                else:
                    fwhm_deconv = 0

                area = i_target['ISOAREA_IMAGE'] * (params_sinfoni.ps ** 2)

                # Find fluxes
                flux_auto = i_target['FLUX_AUTO'] * params_sinfoni.wm2_ergscm2
                bkg = i_target['BACKGROUND'] * params_sinfoni.wm2_ergscm2
                img_erg = img * params_sinfoni.wm2_ergscm2
                img_bkg = img_erg - bkg
                img_2x2 = img_bkg[y_min: (y_min + 2), x_min: (x_min + 2)]
                flux_2x2 = np.sum(img_2x2)

                # Add to catalog
                catalog.add_row([obj, x, y, fwhm, fwhm_deconv, area, flux_auto,
                                 flux_2x2, bkg])

        # Save catalog
        file_out = open(path.join(folder, 'catalog.pkl'), 'wb')
        pickle.dump(catalog, file_out)
        file_out.close()
        ascii.write(catalog, path.join(folder, 'catalog.dat'), overwrite=True)

        # Plot FWHM function
        fwhm = catalog['fwhm_gauss']
        fig = plt.figure(figsize=(10, 5))
        ax = fig.add_subplot(111)
        ax.set_position([0.15, 0.15, 0.7, 0.7])
        bins_fwhm = np.arange(0, (np.max(fwhm) * 1.1), bin_fwhm)
        plt.hist(fwhm, bins=bins_fwhm)
        ax.set_xlabel("FWHM (\")")
        ax.set_ylabel("N")
        plt.savefig(path.join(plot_f, 'fun_fwhm.png'))
        plt.close()

        # Plot integrated flux function
        fluxes_auto = catalog['flux_auto']
        fig = plt.figure(figsize=(10, 5))
        ax = fig.add_subplot(111)
        ax.set_position([0.15, 0.15, 0.7, 0.7])
        bins_fluxes = np.arange(0, (np.max(fluxes_auto) * 1.1), bin_flux_auto)
        plt.hist(fluxes_auto, bins=bins_fluxes)
        ax.set_xlabel("Isophotal flux" +
                      "({0})".format(params_sinfoni.flux_unit_erg))
        ax.set_ylabel("N")
        plt.savefig(path.join(plot_f, 'fun_flux_auto.png'))
        plt.close()

        # Plot aperture flux function
        fluxes_2x2 = catalog['flux_2x2']
        fig = plt.figure(figsize=(10, 5))
        ax = fig.add_subplot(111)
        ax.set_position([0.15, 0.15, 0.7, 0.7])
        bins_fluxes = np.arange(0, (np.max(fluxes_2x2) * 1.1), bin_flux_2x2)
        plt.hist(fluxes_2x2, bins=bins_fluxes)
        ax.set_xlabel("Aperture flux ({0})".format(
            params_sinfoni.flux_unit_erg))
        ax.set_ylabel("N")
        plt.savefig(path.join(plot_f, 'fun_flux_2x2.png'))
        plt.close()

        # Plot area vs flux
        area = catalog['iso_area']
        fig = plt.figure(figsize=(10, 5))
        ax = fig.add_subplot(111)
        ax.set_position([0.15, 0.15, 0.7, 0.7])
        plt.plot(area, fluxes_auto, '.')
        ax.set_xlabel(r"Isophotal area (arcsec$^{2}$)")
        ax.set_ylabel("Isophotal flux" +
                      "({0})".format(params_sinfoni.flux_unit_erg))
        ax.set_xlim(0, area_max)
        ax.set_ylim(0, flux_max)
        plt.savefig(path.join(plot_f, 'iso_area.png'))
        plt.close()

        # Plot FWHM vs flux
        fig = plt.figure(figsize=(10, 5))
        ax = fig.add_subplot(111)
        ax.set_position([0.15, 0.15, 0.7, 0.7])
        plt.plot(fwhm, fluxes_auto, '.')
        ax.set_xlabel("FWHM (\")")
        ax.set_ylabel("Isophotal flux" +
                      "({0})".format(params_sinfoni.flux_unit_erg))
        ax.set_xlim(-0.01, fwhm_max)
        ax.set_ylim(0, flux_max)
        plt.savefig(path.join(plot_f, 'iso_fwhm.png'))
        plt.close()

        # Plot deconvolved FWHM vs flux
        fwhm = catalog['fwhm_deconv']
        fig = plt.figure(figsize=(10, 5))
        ax = fig.add_subplot(111)
        ax.set_position([0.15, 0.15, 0.7, 0.7])
        plt.plot(fwhm, fluxes_auto, '.')
        ax.set_xlabel("Deconvolved FWHM (\")")
        ax.set_ylabel("Isophotal flux" +
                      "({0})".format(params_sinfoni.flux_unit_erg))
        ax.set_xlim(-0.01, fwhm_max)
        ax.set_ylim(0, flux_max)
        plt.savefig(path.join(plot_f, 'iso_fwhm_deconv.png'))
        plt.close()

print("\nProgram terminated\n")
