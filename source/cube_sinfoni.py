"""Functions to analyze reduced SINS/zC-SINF data cubes.
"""


import copy
from os import makedirs, path
import warnings

from matplotlib import pyplot as plt
import numpy as np
from scipy import ndimage

from source import cube_nifs, fit_sinfoni, params_nifs, params_sinfoni


def plot_spectrum(wvl, spectrum, sky=None, fit_mod=None, zoom=None,
                  i_range=None, show_line=True, label_line=True,
                  show_axis=True, y_label=None, title="", close=True,
                  save=None):
    """Plot a 1-D spectrum.
    It can overplot a fitted model. It can gray-out wavelengths that have sky
    lines.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectrum: 1-D Spectrum
        :type spectrum: numpy.ndarray [n] (float)
        :param sky: Telluric lines ranges (min, max) (um)
        :type sky: numpy.ndarray [:, 2] (float), optional
        :param fit_mod: Spectrum fitting model
        :type fit_mod: astropy.modeling.core.spec_prof, optional
        :param zoom: Wavelength zoom limits [min, max] (um)
        :type zoom: list [2] (float), optional
        :param i_range: Spectrum intensity limits [min, max]
        :type i_range: list [2] (float), optional
        :param show_line: Show line center
        :type show_line: bool, default True
        :param label_line: Show line label
        :type label_line: bool, default True
        :param show_axis: Show axis labels
        :type show_axis: bool, default True
        :param y_label: y axis label
        :type y_label: str, optional
        :param title: Plot title
        :type title: str, default ""
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Plot the spectrum
    fig = plt.figure(figsize=(10, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.15, 0.7, 0.7])
    ax.plot(wvl, spectrum, '-k')

    if not zoom:
        zoom = [np.min(wvl), np.max(wvl)]

    ax.set_xlim(zoom)

    if i_range is None:
        spectrum_zoom = spectrum[np.where((wvl >= zoom[0]) & (wvl <= zoom[1]))]
        spectrum_zoom_min = np.min(spectrum_zoom)
        spectrum_zoom_max = np.max(spectrum_zoom)
        spectrum_zoom_range = spectrum_zoom_max - spectrum_zoom_min
        i_range = [(spectrum_zoom_min - (spectrum_zoom_range / 10)),
                   (spectrum_zoom_max + (spectrum_zoom_range / 10))]

    ax.set_ylim(i_range)

    # Show fit
    if fit_mod:
        prof = fit_sinfoni.spec_prof(bb_0=fit_mod.bb_0.value,
                                     bb_1=fit_mod.bb_1.value,
                                     ha_p=fit_mod.ha_p.value,
                                     ha_c=fit_mod.ha_c.value,
                                     ha_s=fit_mod.ha_s.value)

        plt.plot(wvl, prof(wvl), 'r')

        if show_line:
            offset_level = i_range[1]
            offset_sign = -1
            offset_label = 0.1
            plt.plot(([fit_mod.ha_c.value] * 2), plt.ylim(), ':r')

            if label_line:
                text_y_h = offset_level + ((0.2 + offset_label) * offset_sign *
                                           (i_range[1] - i_range[0]))
                plt.text(fit_mod.ha_c.value, text_y_h, r"H$_{\alpha}$",
                         rotation=90, horizontalalignment='right')

    # Show axis labels and ticks
    if y_label:
        ylabel = y_label
    else:
        ylabel = "Flux density"

    if show_axis:
        ax.set_xlabel(r"$\lambda$ ($\mu$m)")
        ax.set_ylabel(ylabel)
    else:
        plt.tick_params(axis='x', bottom=False, labelbottom=False)
        plt.tick_params(axis='y', left=False, labelleft=False)

    # Show sky lines
    if sky is not None:
        for i_sky in range(len(sky)):
            ax.axvspan(sky[i_sky, 0], sky[i_sky, 1], alpha=0.5,
                       facecolor='grey')

    plt.title(title)

    # Save spectrum file
    if save is not None:
        plt.savefig(save)

    # Close the spectrum figure
    if close:
        plt.close()
    else:
        plt.show(block=False)


def plot_map(maps, ps, ticks, param, mask_map=None, mask_thr=0, mask_lim='low',
             vel=False, beam_circ=None, model=None, imin=None, imax=None,
             perc=0, close=True, save=None):
    """Plot the map of a fitted parameter.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.spec_prof)
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param ticks: Distance of the spaxel axes' ticks (")
        :type ticks: float
        :param param: Fitted parameter to map
        :type param: str
        :param mask_map: Fitted parameter to use as mask
        :type mask_map: str, optional
        :param mask_thr: Threshold value for masking
        :type mask_thr: float, default 0
        :param mask_lim: Limit type for masking ('up' for upper, 'low ' for
            lower)
        :type mask_lim: str, default 'low'
        :param vel: Transform the map to velocities
        :type vel: bool, default False
        :param beam_circ: Circles [x, y, radius] of beam (px)
        :type beam_circ: numpy.ndarray [n, 3] (float), optional
        :param model: Parameters of the FWHM ellipse [x0, y0, first FWHM,
            second FWHM, angle, average FWHM] ([px, px, px, px, deg, "])
        :type model: list [6] (float), optional
        :param imin: Minimum intensity value shown
        :type imin: float, optional
        :param imax: Maximum intensity value shown
        :type imax: float, optional
        :param perc: Percentile to remove from the top and bottom of the range
            of values colored (0 for none). Not used if imin or imax are used
        :type perc: float, default 0
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Select map
    map1, title_str = grab_map(maps, param)
    c_label = ""

    if vel:
        lambd_ref = params_nifs.h_alpha
        map1 = params_nifs.c * (((map1 / lambd_ref) ** 2) - 1) / \
            (((map1 / lambd_ref) ** 2) + 1)
        map1 -= np.median(map1)
        map1 /= 1e3
        title_str = title_str.replace("center", "LOS velocity")
        c_label = "Velocity (km/s)"

    if (param[-2:] == '_p') or (param[: 3] == 'bb_'):
        c_label = "Flux density (" + params_sinfoni.flux_dens_unit + ")"
    elif param[-2:] == '_i':
        c_label = "Flux (" + params_nifs.flux_unit + ")"

    # Mask map
    if mask_map:
        mapm, _ = grab_map(maps, mask_map)

        if mask_lim == 'low':
            mapm[np.isnan(mapm)] = -1e100
            mapm_bool = mapm < mask_thr
        else:
            mapm[np.isnan(mapm)] = 1e100
            mapm_bool = mapm > mask_thr

        map1 = np.ma.masked_where(mapm_bool, map1)

    # Show map
    cube_nifs.plot_img(map1, ps, ticks, beam_circ=beam_circ, model=model,
                       imin=imin, imax=imax, c_label=c_label,
                       title=("Map of " + title_str), perc=perc, close=close,
                       save=save)


def grab_map(maps, param):
    """Select the map of a fitted parameter

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.spec_prof)
        :param param: Fitted parameter to map
        :type param: str

    Returns:
        :return map1: Map of a fitting parameter
        :rtype map1: numpy.ndarray [y, x] (float)
        :return title_str: Map title
        :rtype title_str: str
    """

    # Select map
    map1 = np.zeros(maps.shape)
    title_str = ""

    if param == 'bb_0':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].bb_0.value

        title_str = "BB offset"
    elif param == 'bb_1':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].bb_1.value

        title_str = "BB slope"
    elif param == 'ha_p':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].ha_p.value

        title_str = r"H$_{\alpha}$ peak"
    elif param == 'ha_c':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].ha_c.value

        title_str = r"H$_{\alpha}$ center"
    elif param == 'ha_s':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].ha_s.value

        title_str = r"H$_{\alpha}$ $\sigma$"
    elif param == 'ha_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].ha_i.value

        title_str = r"H$_{\alpha}$ integrated"

    return map1, title_str


def convolve_maps(maps, rad):
    """Convolve maps using a circular top-hat kernel of a given diameter.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.spec_prof)
        :param rad: Radius of the circular top-hat kernel (px)
        :type rad: float

    Returns:
        :return maps_conv: Convolved maps of fitted parameters
        :rtype maps_conv: numpy.ndarray [y, x]
            (astropy.modeling.core.spec_prof)
    """

    # Prepare kernel
    side = int(np.floor(rad) * 2) + 1
    center = (side - 1) / 2
    kern = np.zeros([side, side])
    kern_x, kern_y = np.meshgrid(np.arange(side), np.arange(side))
    kern_dist = np.hypot((kern_x - center), (kern_y - center))
    kern[np.where(kern_dist <= rad)] = 1

    # Convolve maps
    parameters = maps[0, 0].param_names
    maps_conv = copy.deepcopy(maps)

    for i_param in range(len(parameters)):
        map1, _ = grab_map(maps, parameters[i_param])
        map1_conv = ndimage.convolve(map1, kern, mode='mirror')
        map1_conv /= np.sum(kern)

        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                if i_param == 0:
                    maps_conv[i_row, i_col].bb_0.value = map1_conv[i_row,
                                                                   i_col]
                elif i_param == 1:
                    maps_conv[i_row, i_col].bb_1.value = map1_conv[i_row,
                                                                   i_col]
                elif i_param == 2:
                    maps_conv[i_row, i_col].ha_p.value = map1_conv[i_row,
                                                                   i_col]
                elif i_param == 3:
                    maps_conv[i_row, i_col].ha_c.value = map1_conv[i_row,
                                                                   i_col]
                elif i_param == 4:
                    maps_conv[i_row, i_col].ha_s.value = map1_conv[i_row,
                                                                   i_col]
                elif i_param == 5:
                    maps_conv[i_row, i_col].ha_i.value = map1_conv[i_row,
                                                                   i_col]

    return maps_conv


def convolve_maps_psf(maps, psf):
    """Convolve maps using a PSF.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.spec_prof)
        :param psf: PSF
        :type psf: numpy.ndarray [y0, x0]

    Returns:
        :return maps_conv: Convolved maps of fitted parameters
        :rtype maps_conv: numpy.ndarray [y, x]
            (astropy.modeling.core.spec_prof)
    """

    # Convolve maps
    parameters = maps[0, 0].param_names
    maps_conv = copy.deepcopy(maps)

    for i_param in range(len(parameters)):
        map1, _ = grab_map(maps, parameters[i_param])
        map1_conv = ndimage.convolve(map1, psf, mode='mirror')
        map1_conv /= np.sum(psf)

        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                if i_param == 0:
                    maps_conv[i_row, i_col].bb_0.value = map1_conv[i_row,
                                                                   i_col]
                elif i_param == 1:
                    maps_conv[i_row, i_col].bb_1.value = map1_conv[i_row,
                                                                   i_col]
                elif i_param == 2:
                    maps_conv[i_row, i_col].ha_p.value = map1_conv[i_row,
                                                                   i_col]
                elif i_param == 3:
                    maps_conv[i_row, i_col].ha_c.value = map1_conv[i_row,
                                                                   i_col]
                elif i_param == 4:
                    maps_conv[i_row, i_col].ha_s.value = map1_conv[i_row,
                                                                   i_col]
                elif i_param == 5:
                    maps_conv[i_row, i_col].ha_i.value = map1_conv[i_row,
                                                                   i_col]

    return maps_conv


def scale_maps(maps_in, ps_in, ps_out):
    """Scale maps changing the pixel scale.

    Parameters:
        :param maps_in: Maps to scale
        :type maps_in: numpy.ndarray [y1, x1] (astropy.modeling.core.spec_prof)
        :param ps_in: Input pixel scale ("/px)
        :type ps_in: float
        :param ps_out: Output pixel scale ("/px)
        :type ps_out: float

    Returns:
        :return maps_out: Scaled maps
        :rtype maps_out: numpy.ndarray [y2, x2]
            (astropy.modeling.core.spec_prof)
    """

    # Prepare scaling
    zoom = ps_in / ps_out
    parameters = maps_in[0, 0].param_names
    map_test, _ = grab_map(maps_in, parameters[0])
    map_test_zoom = ndimage.zoom(map_test, zoom)
    maps_out = np.tile(np.array(fit_sinfoni.spec_prof()), map_test_zoom.shape)
    map1_zoom = np.zeros([len(parameters), map_test_zoom.shape[0],
                          map_test_zoom.shape[1]])

    # Scale maps
    for i_param in range(len(parameters)):
        map1, _ = grab_map(maps_in, parameters[i_param])
        map1_nan = np.isnan(map1)
        map1[map1_nan] = np.median(map1[~map1_nan])
        warnings.simplefilter('ignore')
        map1_zoom[i_param, :, :] = ndimage.zoom(map1, zoom)
        warnings.simplefilter('default')

    for i_row in range(map_test_zoom.shape[0]):
        for i_col in range(map_test_zoom.shape[1]):
            maps_out[i_row, i_col] = \
                fit_sinfoni.spec_prof(bb_0=map1_zoom[0, i_row, i_col],
                                      bb_1=map1_zoom[1, i_row, i_col],
                                      ha_p=map1_zoom[2, i_row, i_col],
                                      ha_c=map1_zoom[3, i_row, i_col],
                                      ha_s=map1_zoom[4, i_row, i_col],
                                      ha_i=map1_zoom[5, i_row, i_col])

    return maps_out


def convolve_psf_cube(cube, psf):
    """Convolve a data cube with a PSF.

    Parameters:
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y0, x0] (float)
        :param psf: PSF
        :type psf: numpy.ndarray [y1, x1] (float)

    Returns:
        :return cube_conv: Convolved data cube
        :rtype cube_conv: numpy.ndarray [n, y0, x0] (float)
    """

    # Convolve cube
    kernel = psf[:, :, None]
    cube_conv = ndimage.convolve(cube, kernel, mode='mirror')

    return cube_conv


def scale_cube(cube_in, ps_in, ps_out):
    """Scale a data cube changing the pixel scale.

    Parameters:
        :param cube_in: Data cube to scale
        :type cube_in: numpy.ndarray [n, x1, y2] (float)
        :param ps_in: Input pixel scale ("/px)
        :type ps_in: float
        :param ps_out: Output pixel scale ("/px)
        :type ps_out: float

    Returns:
        :return cube_out: Scaled data cube
        :rtype cube_out: numpy.ndarray [n, x2, y2] (float)
    """

    # Scale data cube
    zoom = ps_in / ps_out
    warnings.simplefilter('ignore')
    cube_out = ndimage.zoom(cube_in, [1, zoom, zoom])
    warnings.simplefilter('default')

    return cube_out


def bin_vel(wvl, cube, lambd_ref, z, bin_range, vel_range):
    """Bin wavelengths and a 2-D spectrum by velocity channels using the mean
    or median.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param cube: 2-D Spectrum
        :type cube: numpy.ndarray [n, y, x] (float)
        :param lambd_ref: Reference wavelength (um)
        :type lambd_ref: float
        :param z: Guessed redshift
        :type z: float
        :param bin_range: Binning range of velocities (km/s)
        :type bin_range: float
        :param vel_range: Range of velocities to bin [min, max] (km/s)
        :type vel_range: list [2] (float)

    Returns:
        :return vel_bin: Binned velocities
        :rtype vel_bin: numpy.ndarray [m] (float)
        :return cube_bin: Binned 2-D spectrum
        :rtype cube_bin: numpy.ndarray [m, y, x] (float)
    """

    # Bin velocities and spectra
    vel = params_nifs.c * (wvl - lambd_ref) / wvl
    wvl_0 = lambd_ref * (z + 1)
    vel_0 = vel[np.where(np.abs(wvl - wvl_0) ==
                         np.min(np.abs(wvl - wvl_0)))[0][0]]
    vel -= vel_0
    vel_range = np.array(vel_range) * 1e3
    low = vel_range[0]
    vel_bin = np.zeros(wvl.shape)
    cube_bin = np.zeros(cube.shape)
    idx = 0

    while low <= vel_range[1]:
        high = low + (bin_range * 1e3)
        idxs = np.where((vel >= low) & (vel < high))[0]
        vel_bin[idx] = np.mean(vel[idxs])
        cube_bin[idx, :, :] = np.mean(cube[idxs, :, :], axis=0)
        idx += 1
        low = high

    vel_bin = vel_bin[: idx]
    cube_bin = cube_bin[: idx, :, :]

    return vel_bin, cube_bin


def plot_vel(vel, data_vel, ps, ticks, beam_circ=None, model=None, imin=None,
             imax=None, ignore_bottom=False, close=True, save=None):
    """Plot the velocity intensity maps in a cube.

    Parameters:
        :param vel: Velocities
        :type vel: numpy.ndarray [n] (float)
        :param data_vel: 2-D spectrum
        :type data_vel: numpy.ndarray [n, y, x] (float)
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param ticks: Distance of the spaxel axes' ticks (")
        :type ticks: float
        :param beam_circ: Circles [x, y, radius] of beam (px)
        :type beam_circ: numpy.ndarray [n, 3] (float), optional
        :param model: Parameters of the FWHM ellipse [x0, y0, first FWHM,
            second FWHM, angle, average FWHM] ([px, px, px, px, deg, "])
        :type model: list [6] (float), optional
        :param imin: Minimum intensity value shown
        :type imin: float, optional
        :param imax: Maximum intensity value shown
        :type imax: float, optional
        :param ignore_bottom: Ignore the bottom of the FOV to choose
            automatically the maximum color value
        :type ignore_bottom: bool, default False
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename root of the saved images
        :type save: str, optional
    """

    # Manage files
    if not path.exists(save):
        makedirs(save)

    # Select velocities
    c_label = "Flux density (" + params_sinfoni.flux_dens_unit + ")"

    for i_vel in range(len(vel)):

        # Select velocity map
        map1 = data_vel[i_vel, :, :]
        vel_km = int(vel[i_vel] * 1e-3)

        # Show map
        cube_nifs.plot_img(map1, ps, ticks, beam_circ=beam_circ, model=model,
                           imin=imin, imax=imax, c_label=c_label,
                           title="{0} km/s".format(vel_km),
                           ignore_bottom=ignore_bottom, close=close,
                           save=(path.join(save,
                                           'vel_{0}.png'.format(i_vel + 1))))
