"""List of fixed parameters to analyze reduced SINS/zC-SINF data cubes.

References:
    [ref1] Förster Schreiber et al. (2018)
"""

# Fixed parameters
wm2_ergscm2 = 1e3  # Conversion factor from W/m^2 to erg/s/cm^2
flux_unit_erg = r"erg/s/cm$^{2}$"  # Flux units of erg/s/cm^2

# Guessed parameters
peak = 1e-19  # Initial guess of the peak value
min_peak = 0  # Minimum peak value allowed (W/m^2/um), if unconstrained
sigma = 0.0005  # Initial guess of the STD (um)
min_sigma = 1.5  # Fraction of the STD initial guess for minimum value allowed
max_sigma = 8  # Number of the STD initial guess for maximum value allowed

# Förster Schreiber et al. (2018) parameters [ref1]
ps = 0.05  # Datacubes and PSF pixel scale ("/px)
flux_unit = r"W/m$^{2}$"  # Flux units
flux_unit_txt = "W/m^2"  # Flux units for plain text
flux_dens_unit = r"W/m$^{2}$/$\mu$m"  # Flux density units
gal_names = ['Q1623-BX455', 'Q1623-BX502', 'Q1623-BX543', 'Q1623-BX599',
             'Q2343-BX389', 'Q2343-BX513', 'Q2343-BX610', 'Q2346-BX482',
             'Deep3a-6004', 'Deep3a-6397', 'Deep3a-15504', 'K20-ID6',
             'K20-ID7', 'GMASS-2303', 'GMASS-2363', 'GMASS-2540', 'SA12-6339',
             'ZC400528', 'ZC400569', 'ZC400569N', 'ZC401925', 'ZC403741',
             'ZC404221', 'ZC405226', 'ZC405501', 'ZC406690', 'ZC407302',
             'ZC407376', 'ZC407376S', 'ZC407376N', 'ZC409985', 'ZC410041',
             'ZC410123', 'ZC411737', 'ZC412369', 'ZC413507', 'ZC413597',
             'ZC415876']  # Galaxies' names [ref1]
gal_zs = [2.4078, 2.1557, 2.5209, 2.3312, 2.1724, 2.1082, 2.2107, 2.2571,
          2.3871, 1.5133, 2.383, 2.2348, 2.224, 2.4507, 2.452, 1.6149, 2.2971,
          2.3873, 2.2405, 2.2432, 2.1412, 1.4457, 2.2199, 2.287, 2.1539, 2.195,
          2.1819, 2.1729, 2.173, 2.1728, 2.4569, 2.4541, 2.1986, 2.4442,
          2.0281, 2.48, 2.4502, 2.4354]  # Galaxies' redshifts [ref1]
gal_fwhm = [0.11, 0.15, 0.3, 0.25, 0.2, 0.15, 0.24, 0.18, 0.16, 0.19, 0.16,
            0.2, 0.15, 0.17, 0.17, 0.17, 0.14, 0.15, 0.15, 0.15, 0.25, 0.16,
            0.2, 0.24, 0.18, 0.17, 0.16, 0.22, 0.22, 0.22, 0.13, 0.16, 0.18,
            0.19, 0.15, 0.14, 0.17, 0.14]  # Galaxies' PSF FWHM (") [ref1]
r_ap = [0.6, 0.6, 0.85, 0.9, 0.95, 0.6, 0.9, 1, 1.1, 1.1, 0.95, 1, 1, 0.6,
        0.65, 1.25, 0.75, 0.85, 1, 1, 0.75, 0.8, 1, 0.9, 0.9, 1, 0.8, 0.95,
        0.6, 0.4, 0.75, 0.85, 0.85, 0.65, 0.7, 0.85, 0.8, 0.65]  # Photometric
# circular aperture diameter (") [ref1]
f_ha = [9.5, 12.7, 16.8, 28.7, 21, 13.9, 15.4, 13.5, 11.7, 12.7, 15.7, 6.5,
        8.2, 7.6, 4.3, 4.3, 7.4, 15.7, 10, 9.1, 6.9, 12.3, 10.5, 6.2, 6.9,
        30.1, 16.8, 13.6, 9.9, 3.2, 11.2, 9.1, 7, 7.5, 18, 10.1, 8.6, 14.1]
# Circular aperture H_a photometry (10^−17 erg/s/cm^2) [ref1]
