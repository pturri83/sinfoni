"""Functions to fit SINS/zC-SINF data.
"""


import warnings

from astropy.modeling import models, fitting
import numpy as np

from source import params_nifs, params_sinfoni


def fit_spec(wvl, spectr, z, constrain_peak=False):
    """Fit the spectrum to determine line parameters.
    The fit uses a slope for the black body radiation, and one Gaussian the
    H_a.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectr: 1D Spectrum
        :type spectr: numpy.ndarray [n] (float)
        :param z: Guessed redshift
        :type z: float
        :param constrain_peak: Constrained minimium peak value
        :type constrain_peak: bool, default False

    Returns:
        :return spec_out: Fitting model
        :rtype spec_out: astropy.modeling.core.spec_prof
    """

    # Prepare the fitter
    nan_idx = np.where(np.invert(np.isnan(spectr)))
    wvl = wvl[nan_idx]
    spectr = spectr[nan_idx]
    spec_init = spec_prof(ha_p=np.nanmax(spectr),
                          ha_c=(params_nifs.h_alpha * (z + 1)))

    if constrain_peak:
        spec_init.ha_p.bounds = [params_sinfoni.min_peak, 1e10]

    spec_init.ha_s.bounds = [(params_sinfoni.sigma / params_sinfoni.min_sigma),
                             (params_sinfoni.sigma * params_sinfoni.max_sigma)]
    fit_spec_obj = fitting.LevMarLSQFitter()

    # Fit the Gaussian
    warnings.simplefilter('ignore')
    spec_out = fit_spec_obj(spec_init, wvl, spectr)
    warnings.simplefilter('default')

    # Calculate integrated Gaussian H_a
    integr_gauss(spec_out)

    return spec_out


@models.custom_model
def spec_prof(lambd, bb_0=0, bb_1=0, ha_p=params_sinfoni.peak,
              ha_c=(params_nifs.h_alpha *
                    (1 + np.mean(params_sinfoni.gal_zs))),
              ha_s=params_sinfoni.sigma, ha_i=0):
    """Calculate the spectrum profile function.
    The model uses a slope for the black body radiation and a Gaussian for the
    H_a line.

    Parameters:
        :param lambd: Wavelength (um)
        :type lambd: numpy.ndarray [n] (float)
        :param bb_0: Black body intensity offset
        :type bb_0: float, default 0
        :param bb_1: Black body intensity slope, respect to (h_alpha * (1 + z))
        :type bb_1: float, default 0
        :param ha_p: H_a line peak
        :type ha_p: float, default (peak)
        :param ha_c: H_a line center (um)
        :type ha_c: float, default (h_alpha * (1 + z))
        :param ha_s: H_a line STD (um)
        :type ha_s: float, default sigma
        :param ha_i: H_a line integrated Gaussian
        :type ha_i: float, default 0

    Returns:
        :return prof: Spectrum profile
        :rtype prof: numpy.ndarray [n] (float)
    """

    # Calculate line profiles
    bb_prof = bb_0 + (bb_1 * (lambd - (params_nifs.h_alpha *
                                       (1 + np.mean(params_sinfoni.gal_zs)))))
    ha_prof = ha_p * np.exp(-0.5 * (((lambd - ha_c) / ha_s) ** 2))

    # Calculate total profile
    prof = bb_prof + ha_prof
    _ = 0 * ha_i

    return prof


def integr_gauss(spec):
    """Calculate the Gaussian integrated lines.

    Parameters:
        :param spec: Fitting model
        :type spec: astropy.modeling.core.spec_prof
    """

    # Calculate the Gaussian integral
    ha_i = spec.ha_p * spec.ha_s * np.sqrt(2 * np.pi)
    spec.ha_i = ha_i


def fit_map(wvl, cube, z, constrain_peak=False):
    """Calculate the fitted parameter maps of a data cube, spaxel by spaxel.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y, x] (float)
        :param z: Guessed redshift
        :type z: float
        :param constrain_peak: Constrained minimium peak value
        :type constrain_peak: bool, default False

    Returns:
        :return maps: Map of fitting models
        :rtype maps: numpy.ndarray [y, x] (astropy.modeling.core.spec_prof)
    """

    # Prepare output
    maps = np.tile(np.array(spec_prof()), [cube.shape[1], cube.shape[2]])

    # Scan the map
    for i_row in range(cube.shape[1]):
        for i_col in range(cube.shape[2]):
            spectr = cube[:, i_row, i_col]

            if np.all(np.isnan(cube[:, i_row, i_col])):
                spec_param = spec_prof(ha_p=0)
            else:
                spec_param = fit_spec(wvl, spectr, z,
                                      constrain_peak=constrain_peak)

            maps[i_row, i_col] = spec_param

    return maps
